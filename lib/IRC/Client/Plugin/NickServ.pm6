#! /usr/bin/env false

use v6.c;

use Config;
use IRC::Client;

#| The IRC::Client::Plugin to deal with NickServ interaction.
class IRC::Client::Plugin::NickServ does IRC::Client::Plugin
{
	has Config $.config;

	#| Identify with NickServ. This is done on IRC code 376 (end of MOTD),
	#| since this is what most servers accept as the earliest time to start
	#| interacting with the server.
	method irc-n376($e)
	{
		# Extract the config parameters
		my Str $user = $!config<nickserv><nickname> // $!config<bot><nickname>;
		my Str $pass = $!config<nickserv><password>;

		# Nothing to do if we don't have a username and a password
		if (!$user || !$pass) {
			note "Missing username or password for NickServ auth";
			return;
		}

		# Send the identify command
		$e.irc.send-cmd: "NS identify $user $pass";
	}

	multi method irc-notice($e where m:i/"you are now identified"/)
	{
		return if $e.server.current-nick eq $e.server.nick.first;

		# Ghost our nick
		$e.irc.send-cmd: "NS GHOST {$e.server.nick.first}";
	}

	multi method irc-notice($e where m:i/"has been ghosted"/)
	{
		# Use our nick
		$e.irc.nick: $e.server.nick.first;
	}
}

# vim: ft=perl6 noet
